package ca.jhoffman.demospinner;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class ItemsSpinnerAdapter extends ArrayAdapter<String> {

    private ArrayList<Item> items;

    public ItemsSpinnerAdapter(@NonNull Context context) {
        super(context, android.R.layout.simple_list_item_1);
        // Preciser le layout a utiliser parmi ceux du systeme
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        Item item = items.get(position);

        return item.name;
        // Formater l'objet pour afficher en string
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);

        Item item = items.get(position);
        view.setText(item.name);

        return view;
    }
}
