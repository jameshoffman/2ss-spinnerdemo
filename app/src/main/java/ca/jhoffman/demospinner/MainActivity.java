package ca.jhoffman.demospinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Item> items = new ArrayList<>();
    ItemsSpinnerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        items.add(new Item("AAA", "A description"));
        items.add(new Item("BBB", "B description"));
        items.add(new Item("CCC", "C description"));

        Spinner spinner = findViewById(R.id.spinner);

        adapter = new ItemsSpinnerAdapter(this);
        adapter.setItems(items);
        spinner.setAdapter(adapter);

        spinner.setSelection(0,false);
        // hack pour eviter que le selected listener soit appelé toujours 1 fois à l'initialisation

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Item item = items.get(i);

                Toast.makeText(MainActivity.this, "Clicked: " + item.name + ", " + item.description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }
}